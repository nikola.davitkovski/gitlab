# frozen_string_literal: true
require 'spec_helper'

RSpec.describe LicenseCompliance::CollapsedComparerEntity do
  it_behaves_like 'comparer entity'
end
